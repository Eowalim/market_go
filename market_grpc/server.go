package market_grpc

import (
	context "context"
	"corentin/banking_app/market"
)

type Server struct {
	UnimplementedMarketServiceServer
}

func (s Server) CreateMarket(ctx context.Context, req *Request) (*Response, error) {
	market := market.Market{
		Name:          req.GetName(),
		InitialValue:  int(req.GetInitialValue()),
		CurrentValue:  int(req.GetCurrentValue()),
		NbFluctuation: int(req.GetNbFluctuation()),
	}

	err := market.StoreToDatabase()

	if err != nil {
		return nil, err
	}

	return nil, nil
}
