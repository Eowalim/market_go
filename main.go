package main

import (
	"bufio"
	"corentin/banking_app/market"
	"corentin/banking_app/market_grpc"
	"log"
	"net"
	"os"

	"google.golang.org/grpc"
)

func main() {
	file, err := os.Open("data.txt")
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Scan()

	markets, err := market.InitializeMappedValues(scanner.Text())
	if err != nil {
		log.Fatal(err)
	}

	for scanner.Scan() {
		err = market.ComputeFluctuation(scanner.Text(), markets)
		if err != nil {
			log.Fatal(err)
		}
	}

	for _, market := range markets {
		market.PrettyPrint()
		err := market.StoreToDatabase()
		if err != nil {
			log.Fatal(err)
		}
	}

	lis, err := net.Listen("tcp", "0.0.0.0:8082")
	if err != nil {
		log.Fatal(err)
	}

	serverRegistrar := grpc.NewServer()
	server := &market_grpc.Server{}
	market_grpc.RegisterMarketServiceServer(serverRegistrar, server)
	if err := serverRegistrar.Serve(lis); err != nil {
		log.Fatal(err)
	}

}
