package market_test

import (
	"corentin/banking_app/market"
	"log"
	"testing"
)

func Test_InitialiazeMarketOK(t *testing.T) {
	testData := "CAC40:7500;DAX:17450;FTSE100:7993;IBEX35:10686"
	markets, err := market.InitializeMappedValues(testData)
	if err != nil {
		log.Fatal(err)
	}

	for _, market := range markets {
		if market.Name == "CAC40" {
			if market.CurrentValue != 7500 {
				t.Log("Current value is not what wa expected")
				t.Fail()
			}
		}
	}
}
