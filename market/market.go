package market

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Market struct {
	Name          string `bson:"name"`
	InitialValue  int    `bson:"initial_value"`
	CurrentValue  int    `bson:"current_value"`
	NbFluctuation int    `bson:"nb_fluct"`
}

func (m Market) PrettyPrint() {
	fmt.Printf("Market %s started at %d, changed %d times and  is now at: %d\n", m.Name, m.InitialValue, m.NbFluctuation, m.CurrentValue)
}

func (m Market) StoreToDatabase() error {
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI("mongodb://root:root@localhost:27017/"))
	if err != nil {
		log.Fatal(err)
	}

	coll := client.Database("banking").Collection("market")
	_, err = coll.InsertOne(context.Background(), m, nil)

	if err != nil {
		log.Fatal(err)
	}
	return nil
}

func InitializeMappedValues(data string) ([]Market, error) {
	var markets []Market

	values := strings.Split(data, ";")

	for i := 0; i < len(values); i++ {
		splitted := strings.Split(values[i], ":")
		iValue, err := strconv.Atoi(splitted[1])
		if err != nil {
			return nil, err
		}

		markets = append(markets, Market{
			Name:          splitted[0],
			InitialValue:  iValue,
			CurrentValue:  iValue,
			NbFluctuation: 0,
		})
	}

	return markets, nil
}

func ComputeFluctuation(data string, markets []Market) error {
	values := strings.Split(data, ";")

	for index, market := range markets {
		for _, values := range values {
			splitted := strings.Split(values, ":")
			if splitted[0] == market.Name {
				iValue, err := strconv.Atoi(splitted[1])
				if err != nil {
					return err
				}
				newValue := market.CurrentValue + iValue
				market.CurrentValue = newValue
				market.NbFluctuation++
				markets[index] = market
			}
		}
	}

	return nil

}
